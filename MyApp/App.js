import * as React from 'react';
import { ImageBackground, Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
// import { io } from 'socket.io-client';
// import { useEffect } from 'react';
import CustomButton from './components/CustomButton';
import Game from './components/Game';
import PlayButton from './components/PlayButton';
import Storage from './components/Storage';

// const socket = io('http://localhost:9000', {
//   autoConnect: false,
//   transports: ["websocket"]
// });

function HomeScreen({ navigation }) {
  // useEffect(() => {
  //   socket.connect();
  //   socket.emit('chat message', 'Vous avez un nouveau message.');
  //   socket.on('chat message', (msg) => {
  //     console.log(msg);
  //   });
  // }, []);

  return (
    <View style={{ flex: 1 }}>
      <ImageBackground
        source={require('./assets/img/LaRouteEstLongue-FondDEcran.png')}
        style={{ flex: 1, width: '100%', height: '100%' }}
        resizeMode='cover'
      >
        <View style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'center' }}>
          <PlayButton title="Jouer" onPress={() => navigation.navigate('Game')} />
          <View style={{ flexDirection: 'row', marginBottom: 50 }}>
            <CustomButton title="Règles" onPress={() => navigation.navigate('Rules')} />
            <CustomButton title="Crédits" onPress={() => navigation.navigate('Credits')} />
          </View>
        </View>
      </ImageBackground>
    </View>
  );

}

function GameScreen({ navigation }) {
  // useEffect(() => {
  //   fetch('http://localhost:3000/utilisateurs').then(res => res.json()).then(data => console.log(data))
  // }, []);

  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <ImageBackground source={require('./assets/img/LaRouteEstLongue-FondDEcran2.png')} style={{ flex: 1, width: '100%', height: '100%', alignItems: 'stretch', alignItems: 'center', justifyContent: 'center' }} resizeMode='cover'>
        <View style={{ flex: 0.7 }}>
          <Game></Game>
        </View>
        <View style={{ flex: 0.3, alignItems: 'center', justifyContent: 'center' }}>
          <PlayButton title="Relancer" onPress={() => navigation.push('Game')} />
          <View style={{ flexDirection: 'row' }}>
            <CustomButton title="Règles" onPress={() => navigation.navigate('Rules')} />
            <CustomButton title="Retour à l'accueil" onPress={() => navigation.navigate('Home')} />
          </View>
        </View>
      </ImageBackground>
    </View>
  );
}

function RulesScreen({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <ImageBackground source={require('./assets/img/LaRouteEstLongue-FondDEcran.png')} style={{ flex: 1, width: '100%', height: '100%', alignItems: 'stretch', alignItems: 'center', justifyContent: 'center' }} resizeMode='cover'>
        <Text style={{ letterSpacing: 2, margin: 10, textAlign: 'center', fontWeight: 'bold' }}>Le joueur peut retourner deux cartes à la fois.</Text>
        <Text style={{ letterSpacing: 2, margin: 10, textAlign: 'center', fontWeight: 'bold' }}>Si les deux cartes retournées sont identiques, elles restent face visible.</Text>
        <Text style={{ letterSpacing: 2, margin: 10, textAlign: 'center', fontWeight: 'bold' }}>Si les deux cartes retournées ne sont pas identiques, elles sont retournées face cachée après un court délai.</Text>
        <Text style={{ letterSpacing: 2, margin: 10, textAlign: 'center', fontWeight: 'bold' }}>Le but est de retourner toutes les cartes en un nombre limité d'essais.</Text>
        <Text style={{ letterSpacing: 2, margin: 10, marginBottom: 50, textAlign: 'center', fontWeight: 'bold' }}>Vous avez dix essais pour y parvenir.</Text>
        <View style={{ alignItems: 'center', justifyContent: 'center' }}>
          <PlayButton title="Jouer" onPress={() => navigation.navigate('Game')} />
          <CustomButton title="Retour à l'accueil" onPress={() => navigation.navigate('Home')} />
        </View>
      </ImageBackground>
    </View>);
}

function CreditsScreen({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <ImageBackground source={require('./assets/img/LaRouteEstLongue-FondDEcran.png')} style={{ flex: 1, width: '100%', height: '100%', alignItems: 'stretch', alignItems: 'center', justifyContent: 'center' }} resizeMode='cover'>
        <Text style={{ letterSpacing: 2, margin: 10, textAlign: 'center', fontWeight: 'bold' }}>Yaël Cubizolles</Text>
        <Text style={{ letterSpacing: 2, margin: 10, textAlign: 'center', fontWeight: 'bold' }}>MMI3 DW-DI</Text>
        <Text style={{ letterSpacing: 2, margin: 10, marginBottom: 50, textAlign: 'center', fontWeight: 'bold' }}>IUT Le Puy-en-Velay</Text>
        <View style={{ alignItems: 'center', justifyContent: 'center', margin: '50px' }}>
          <CustomButton title="Retour à l'accueil" onPress={() => navigation.navigate('Home')} />
        </View>
      </ImageBackground>
    </View >
  );
}

function ScoresScreen({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <ImageBackground source={require('./assets/img/LaRouteEstLongue-FondDEcran2.png')} style={{ flex: 1, width: '100%', height: '100%', alignItems: 'stretch', alignItems: 'center', justifyContent: 'center' }} resizeMode='cover'>
        <View style={{ flex: 0.7 }}>
          <Storage></Storage>
        </View>
        <View style={{ flex: 0.3, alignItems: 'center', justifyContent: 'center' }}>
          <PlayButton title="Relancer" onPress={() => navigation.push('Game')} />
          <View style={{ flexDirection: 'row' }}>
            <CustomButton title="Règles" onPress={() => navigation.navigate('Rules')} />
            <CustomButton title="Retour à l'accueil" onPress={() => navigation.navigate('Home')} />
          </View>
        </View>
      </ImageBackground>
    </View>
  )
}

const Stack = createNativeStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={HomeScreen} options={{ headerShown: false, title: 'Jeu de mémoire' }} />
        <Stack.Screen name="Game" component={GameScreen} options={{ headerShown: false, title: 'Jeu' }} />
        <Stack.Screen name="Rules" component={RulesScreen} options={{ headerShown: false, title: 'Règles' }} />
        <Stack.Screen name="Credits" component={CreditsScreen} options={{ headerShown: false, title: 'Crédits' }} />
        <Stack.Screen name="Scores" component={ScoresScreen} options={{ headerShown: false, title: 'Scores' }} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;