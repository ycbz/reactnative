import React, { useEffect, useState } from 'react';
import { ScrollView, StyleSheet, Text, View } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useFocusEffect } from '@react-navigation/native';

const Storage = () => {
    const [games, setGames] = useState([]);

    useFocusEffect(
        React.useCallback(() => {
            const getSavedData = async () => {
                const savedGames = await AsyncStorage.getItem("games");

                if (savedGames !== null) {
                    setGames(JSON.parse(savedGames));
                }
            };

            getSavedData();
        }, [])
    );

    return (
        <View style={{ flex: 1, marginTop: 50 }}>
            <ScrollView contentContainerStyle={{ ...styles.container, flexGrow: 1 }}>
                {Array.isArray(games) && [...games].reverse().map((game, index) => (
                    <View key={index} style={styles.game}>
                        <Text style={styles.text}>{game.result}</Text>
                        {game.result === 'Victoire' && <Text style={styles.text}>Essais restants : {game.remainingAttempts}</Text>}
                    </View>
                ))}
            </ScrollView>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    game: {
        marginBottom: 20,
    },
    text: {
        fontSize: 24,
        fontWeight: 'bold',
        color: '#0b0e29',
    }
});

export default Storage;