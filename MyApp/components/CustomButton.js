import React from 'react';
import { TouchableOpacity, Text, StyleSheet } from 'react-native';

const CustomButton = ({ title, onPress }) => {
    return (
        <TouchableOpacity style={styles.button} onPress={onPress}>
            <Text style={styles.buttonText}>{title}</Text>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    button: {
        backgroundColor: '#0b0e29',
        borderRadius: 20,
        justifyContent: 'center',
        margin: 10,
        padding: 16,
        width: "auto"
    },
    buttonText: {
        color: '#fef3e0',
        fontSize: 16,
        letterSpacing: 2,
        textAlign: 'center'
    },
});

export default CustomButton;