import React from 'react';
import { TouchableOpacity, Text, StyleSheet } from 'react-native';

const PlayButton = ({ title, onPress }) => {
    return (
        <TouchableOpacity style={styles.button} onPress={onPress}>
            <Text style={styles.buttonText}>{title}</Text>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    button: {
        backgroundColor: '#ff8c00',
        borderRadius: 20,
        margin: 10,
        padding: 16,
        width: "auto"
    },
    buttonText: {
        color: '#0b0e29',
        fontSize: 30,
        fontWeight: 'bold',
        letterSpacing: 2,
        textAlign: 'center'
    },
});

export default PlayButton;