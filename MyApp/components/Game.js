import React, { useEffect, useState } from 'react';
import { ImageBackground, View, Text, TouchableOpacity, StyleSheet, FlatList } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import CustomButton from './CustomButton';

const symbols = [
    require('../assets/img/LaRouteEstLongue.jpg'),
    require('../assets/img/CEQQISE.jpg'),
    require('../assets/img/CoeurBlanc.jpg'),
    require('../assets/img/Extraterrestre.jpg'),
    require('../assets/img/Independance.jpg'),
    require('../assets/img/DemainCaIra.jpg'),
    require('../assets/img/LoinDuMonde.jpg'),
    require('../assets/img/LaMachine.jpg')
];

const generateCards = () => {
    const pairs = symbols.concat(symbols);
    return pairs.map((symbol, index) => ({ id: index, symbol, isMatched: false }));
};

const shuffleArray = (array) => {
    const shuffledArray = [...array];
    for (let i = shuffledArray.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [shuffledArray[i], shuffledArray[j]] = [shuffledArray[j], shuffledArray[i]];
    }
    return shuffledArray;
};

const Game = () => {
    const [flippedCards, setFlippedCards] = useState([]);
    const [cards, setCards] = useState(shuffleArray(generateCards()));

    const handleCardPress = (id, symbol) => {
        if (flippedCards.length < 2) {
            setFlippedCards((prevFlippedCards) => [...prevFlippedCards, { id, symbol }]);
        }
    };

    const [gameOver, setGameOver] = useState(false);

    const [attempts, setAttempts] = useState(10);

    const navigation = useNavigation();

    useEffect(() => {
        if (flippedCards.length === 2) {
            if (flippedCards[0].symbol === flippedCards[1].symbol) {
                setCards((prevCards) => {
                    return prevCards.map((card) => {
                        if (card.id === flippedCards[0].id || card.id === flippedCards[1].id) {
                            return { ...card, isMatched: true };
                        }
                        return card;
                    });
                });
                setFlippedCards([]);
                const allMatched = cards.every((card) => card.isMatched || card.id === flippedCards[0].id || card.id === flippedCards[1].id);
                if (allMatched) {
                    setGameOver(true);
                }
            } else {
                setTimeout(() => {
                    setFlippedCards([]);
                }, 1000);
                setAttempts((prevAttempts) => prevAttempts - 1);
            }
        }
    }, [flippedCards]);

    const renderCard = ({ item }) => {
        const isFlipped = flippedCards.some((flippedCard) => flippedCard.id === item.id) || item.isMatched;

        return (
            <TouchableOpacity
                style={[
                    styles.card,
                    isFlipped && styles.flipped,
                    isFlipped && flippedCards.length === 2 && styles.mismatched,
                    item.isMatched && styles.matched
                ]}
                onPress={() => handleCardPress(item.id, item.symbol)}
                disabled={isFlipped || (flippedCards.length === 2 && !isFlipped)}
            >
                {isFlipped ? (
                    <ImageBackground source={item.symbol} style={[styles.symbol, item.isMatched && styles.matchedSymbol]} />
                ) : null}
            </TouchableOpacity>
        );
    };

    useEffect(() => {
        if (attempts === 0) {
            setGameOver(true);
        }
    }, [attempts]);

    if (gameOver) {
        // Obtenez les jeux précédents de AsyncStorage
        AsyncStorage.getItem("games").then(async (value) => {
            var games = JSON.parse(value) || [];

            // Créez un nouvel objet game avec gameOver et attempts
            var game = { result: gameOver ? 'Victoire' : 'Défaite', remainingAttempts: attempts };

            // Ajoutez le nouvel objet game au début du tableau games
            games.unshift(game);

            // Sauvegardez le tableau games dans AsyncStorage
            await AsyncStorage.setItem("games", JSON.stringify(games));
        });
    }

    return (
        <View style={styles.container}>
            {gameOver ? (
                attempts > 0 ? (
                    <>
                        <Text style={styles.gameOverText}>Victoire.</Text>
                        <CustomButton title="Scores" onPress={() => navigation.navigate('Scores')} />
                    </>
                ) : (
                    <>
                        <Text style={styles.gameOverText}>Défaite.</Text>
                        <CustomButton title="Scores" onPress={() => navigation.navigate('Scores')} />
                    </>
                )
            ) : (
                <>
                    <Text style={styles.attemptsText}>Essais restants : {attempts}</Text>
                    <FlatList
                        data={cards}
                        renderItem={renderCard}
                        keyExtractor={(item) => item.id.toString()}
                        numColumns={4}
                    />
                </>
            )}
        </View>
    );
};

const styles = StyleSheet.create({
    attemptsText: {
        fontSize: 18,
        fontWeight: 'bold',
        color: '#0b0e29',
        margin: 50,
    },
    container: {
        alignItems: 'center',
        flex: 1,
        justifyContent: 'center'
    },
    card: {
        alignItems: 'center',
        aspectRatio: 1,
        backgroundColor: 'transparent',
        borderColor: '#0b0e29',
        borderRadius: 4,
        borderWidth: 2,
        justifyContent: 'center',
        margin: 7.5,
        width: 80
    },
    flipped: {
        backgroundColor: 'transparent',
        borderColor: 'white',
    },
    gameOverText: {
        fontSize: 24,
        fontWeight: 'bold',
        color: '#0b0e29',
    },
    matched: {
        backgroundColor: 'transparent',
        borderColor: '0b0e29',
    },
    symbol: {
        height: '100%',
        resizeMode: 'cover',
        width: '100%'
    }
});

export default Game;